<?php


namespace App\Core;


use PDO;

class DatabaseConnexion {
	/** @var DatabaseConnexion Instance unique de PDO */
	private static $instance = null;
	/** @var PDO */
	private $db;

	/**
	 * Model constructor.
	 * Design pattern Singleton
	 */
	protected function __construct() {
		try {
			$configs = CONFIG['db'];

			$dns = $configs['driver'].':host='.$configs['host'].';port='.$configs['port'].';dbname='.$configs['name'].";charset=".$configs['charset'];;
			$options = $configs['default_options'];

			$this->db = new PDO($dns, $configs['user'], $configs['pass'], $options);
		}
		catch(PDOException $e){
			die('Connexion à la base de données impossible.<br> Erreur : '. $e->getMessage());
		}
	}

	public static function getDb(){
		if(!isset(self::$instance)){
			self::$instance = new self();
		}
		return self::$instance->db;
	}
}
<?php
	spl_autoload_register(function ($className){
		$classFile = ROOT . str_replace('App','app',str_replace('\\', DS, $className)) . '.php';
		if(file_exists($classFile)) {
			require_once $classFile;
		}
	});

<?php

namespace App\Core;

use App\Common\User;
use\App\Common\Authentificateur;
use App\Controller\ErrorController;
use App\Renderer\DefaultRenderer;
use App\Renderer\RendererInterface;
use App\Repository\UserRepository;
use Exception;

/**
 * Class Application
 * @package App\Core
 * @property-read Router $router
 * @property-read RendererInterface $renderer
 * @property string $view
 * @property-read array $data
 * @property-read Authentificateur $authen
 * @property-read User $user
 */
class Application {
	/** @var Router */
	private $router;

	/** @var Authentificateur */
	private $authen;

	/** @var string */
	public $view;

	/** @var array */
	private $data;

	/** @var RendererInterface */
	private $renderer;

    /** @var User */
    private $user;

	/**
	* @param $name
	* @return mixed
	* @throws Exception
	*/
	public function __get($name){
		if(property_exists($this,$name))
			return $this->$name;
		else
			throw new Exception("Propriété non définie ou inaccessible");
	}

	public function __construct() {
		$this->renderer = new DefaultRenderer();

		$this->authen = new Authentificateur();

		$repoUser = new UserRepository();
		$this->user = $repoUser->getByEmail($this->authen->getCurrentUser());

		$this->router = new Router($_GET['url']??null);
		$this->view = $this->router->controller . '/' . $this->router->action;

		if(class_exists($this->router->formattedController)){
			$controller = new $this->router->formattedController($this);

			if(method_exists($controller, $this->router->formattedAction)){
				$this->data = call_user_func_array( [$controller, $this->router->formattedAction], $this->router->params) ?? [];

				$this->renderer->render($this);
				return;
			}
		}

		// Controller ou Action inexistants : Erreur 404
		http_response_code(404);
		$this->data = (new ErrorController($this))->index() ?? [];
		$this->view = 'error/error404';
		$this->renderer->render($this);
	}

	/**
	 * @param RendererInterface $renderer
	 */
	public function setRenderer(RendererInterface $renderer): void {
		$this->renderer = $renderer;
	}
}
<?php


namespace App\Controller;

use App\Lib\Helper;
use App\Model\PanierModel;
use App\Renderer\JsonRenderer;
use App\Repository\UserRepository;
use Exception;
use PDOException;


class UserController extends AbstractController {

    public function index() {
    	Helper::redirect(URL.'user/show-register');
    }


    public function connexion()
    {
        $this->app->setRenderer(new JsonRenderer());
        $repoUser = new UserRepository();

        if ($this->app->authen->isLogged())
            $err = "déjà logged";
        else {
            if (!empty($_POST)) {
                $email = $_POST['email'] ?? false;
                $mdp   = $_POST['mdp'] ?? false;

                if ($email && $mdp) {
                    if ($repoUser->verifyPassword($email, $mdp)) {
                        $this->app->authen->login($email);
                        $this->sessionToDB($email);
                    } else {
                        $err = "Authentification impossible";
                    }
                } else {
                    $err = "Certains champs ne sont pas remplis";
                }
            } else {
				$err = "Certains champs ne sont pas remplis";
            }
           
        }
        return [
            'error' => $err ?? false
        ];
    }

    public function sessionToDB($email) {
        $repoUser = new UserRepository();
        $panierModel = PanierModel::getModel();
        $user = $repoUser->getByEmail($email);
        $panier = $panierModel->getUserPanier($user->getId());
        $isAlreadyInPanier = true;
        foreach ($_SESSION["panier"] as $sessionKey => $sessionArticle) {
            foreach ($panier as $panierKey => $panierArticle) {
                if ($panierArticle['idArticle'] == $sessionArticle['idArticle']) {
                    if ((int)$panierArticle['qte']+(int)$sessionArticle['qte'] > (int)$panierArticle['qteArticle']) {
                        $panierModel->updatePanierQte ((int)$user->getId(), (int)$panierArticle['idArticle'], (int)$panierArticle['qteArticle']);
                    }
                    else {
                        $panierModel->updatePanierQte ((int)$user->getId(), (int)$panierArticle['idArticle'], (int)$panierArticle['qte']+(int)$sessionArticle['qte']);
                    }
                    $isAlreadyInPanier = false;
                }
            }
            if ($isAlreadyInPanier) {
                $panierModel->addPanier((int)$user->getId(), (int)$sessionArticle['idArticle'], (float)$sessionArticle['qte']);
            }
            $isAlreadyInPanier = true;
        }
        $_SESSION["panier"] = [];
    }

    public function showRegister() {
    	if(!$this->app->user->isGuest())
    		Helper::redirect(URL);
        return [];
    }

    public function register() {
        
        $this->app->setRenderer(new JsonRenderer());

        if($this->app->authen->isLogged())
            Helper::redirect(URL);
        else {
            if (!empty($_POST)) {
            	$repoUser = new UserRepository();

                $email  = strtolower(trim($_POST['email'] ?? false));
                $nom    = trim($_POST['nom']    ?? false);
                $prenom = trim($_POST['prenom'] ?? false);
                $mdp1   = trim($_POST['mdp1']   ?? false);
                $mdp2   = trim($_POST['mdp2']   ?? false);

                if ($email!=="" &&
					$nom!=="" &&
					$prenom!=="" &&
					$mdp1!=="" &&
					$mdp2!=="") {

					if(filter_var($email, FILTER_VALIDATE_EMAIL) && strlen($email)<256) {
						if(strlen($nom)<256) {
							if(strlen($prenom)<256) {
								if(strlen($mdp1)<256) {
									if($mdp1===$mdp2) {
										try {
											$repoUser->add($email, $nom, $prenom, $mdp1);
										}
										catch(PDOException $e) {
											if($e->errorInfo[1] == 1062) {
												$erreur = "Cet identifiant est déjà pris";
											}
											else {
												$erreur = "Erreur lors de l'insertion en base (err".$e->errorInfo.')';
											}
										}
										catch(Exception $e){
											$erreur = $e->getMessage();
										}
									}
									else {
										$erreur = "Les mots de passe sont différents";
									}
								} else {
									$erreur = "Mot de passe invalide";
								}
							} else {
								$erreur = "Prénom invalide";
							}
						} else {
							$erreur = "Nom invalide";
						}
					} else {
						$erreur = "Email invalide";
					}
                } else {
                    $erreur = "Certains champs requis sont vides";
                }
            }
        }
        return [
            'error' => $erreur ?? false
        ];
    }

    public function logout(){
        if ($this->app->authen->isLogged()){
            $this->app->authen->logout();
        }
		Helper::redirect(URL);
	}
}
<?php


namespace App\Controller;


use App\Lib\Helper;
use App\Model\ModelAffichage;
use App\Repository\ArticlesRepository;
use App\Repository\TriDefautRepository;

class IndexController extends AbstractController {

	public function __construct($app) {
		parent::__construct($app);
		if($this->app->user->isAdmin())
			Helper::redirect(URL.'admin/');
	}

	public function index() {
	    $model=ModelAffichage::getModel();
	    $repoTri = new TriDefautRepository();

	    //input min / max / catégorie
        $min = (isset($_GET['min']) && trim($_GET['min'])!=="") ? (double)$_GET['min'] : null;
        $max = (isset($_GET['max']) && trim($_GET['max'])!=="") ? (double)$_GET['max'] : null;
        $ids = (isset($_GET['cat']) && is_array($_GET['cat']))  ? $_GET['cat'] : [];


        // inputs tri
		$triRegex = '#^('.TriDefautRepository::TRI_NOM.'|'.TriDefautRepository::TRI_PRIX.')-('
					.TriDefautRepository::TRI_CROISSANT.'|'.TriDefautRepository::TRI_DECROISSANT.')$#';
        if(isset($_GET['tri']) &&
		   preg_match($triRegex,$_GET['tri'])){
        	$tri = explode('-', $_GET['tri']);
		}

        //récup data
        $data = $model->getArticlesFiltered($_GET['cat']??[], $min, $max, $tri[0]??null, $tri[1]??null);

        //pagination
		$nbPages = ceil(count($data)/10);
		$page = filter_var($_GET['page']??1, FILTER_VALIDATE_INT,[ 'options' => [
			'min_range' => 1,
			'max_range' => $nbPages,
			'default'   => 1
		]]);

		$data = array_filter($data, function ($key) use ($page){
			return $key>=($page-1)*10 && $key<=$page*10-1;
		}, ARRAY_FILTER_USE_KEY);

        $urlParam = array_filter($_GET, function ($key){
        	return in_array($key, ['cat','min','max','tri']);
		}, ARRAY_FILTER_USE_KEY);

		return [
		    'Article'=> $data,
            'categories' => $model->getAllCategories(),
            'isChecked' => $ids,
            'min' => $min,
            'max' => $max,
			'curTri' => implode('-', $tri ??  $repoTri->get(true)),
			'nbPages' => $nbPages,
			'curPage' => $page,
			'curGet' => http_build_query($urlParam),
		];
	}
}
<?php


namespace App\Controller;


use App\Core\Application;

abstract class AbstractController {
	/** @var Application */
	protected $app;

	public function __construct($app) {
		$this->app = $app;
	}

	abstract public function index();
}
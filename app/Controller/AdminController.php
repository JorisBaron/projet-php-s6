<?php


namespace App\Controller;


use App\Lib\Helper;
use App\Model\ModelArticles;
use App\Repository\TriDefautRepository;
use Exception;


class AdminController extends AbstractController {

	public function __construct($app) {
		parent::__construct($app);
		if(!$this->app->user->isAdmin())
			Helper::redirect(URL);
	}

	public function index() {
		$this->app->view = 'admin/ajouter-article';
		return $this->ajouterArticle();
	}

	public function ajouterArticle(){
		$model= ModelArticles::getModel();
		
		if (!empty($_POST)) {
			$nom = $_POST['nom'] ?? false;
			$qte = (int)$_POST['qte'] ?? false;
			$cat = (int)$_POST['cat'] ?? false;
			$prix = (double)$_POST['prix'] ?? false;
			$hasImage = isset($_FILES['image']['tmp_name']) && trim($_FILES['image']['tmp_name']!=="");


			if ($nom!==false && $qte!==false && $cat!==false && $prix!==false) {
				if(trim($nom) ==""){
					$msg = [
						'msg' => "Le nom est vide",
						'type' => 'danger'
					];
				}
				else if($qte <= 0){
					$msg = [
						'msg' => "La quantité doit être strictement positive",
						'type' => 'danger'
					];
				}
				elseif($prix<=0){
					$msg = [
						'msg' => "Le prix doit être strictement positif",
						'type' => 'danger'
					];
				}
				else {
					try {

						$imgFile = $hasImage ? $this->processUploadedArticleImage('image') : null;
						$model->addArticle($nom, $qte, $cat, $prix, $imgFile);
						$msg = [
							'msg' => "Article ajouté avec succès",
							'type' => 'success'
						];
					} catch (\PDOException $e) {
						if ($e->errorInfo[1] == 1216) {
							$msg = [
								'msg' => "Cette catégorie n'est pas valide",
								'type' => 'danger'
							];
						} else {
							$msg = [
								'msg' => "Erreur lors de l'insertion en base (err " . $e->errorInfo . ')',
								'type' => 'danger'
							];
						}
					}
					catch(Exception $e) {
						$msg = [
							'msg' => $e->getMessage(),
							'type' => 'danger'
						];
					}
				}
			} else {
				$msg = [
					'msg' => "Certains champs requis sont vides",
					'type' => 'danger'
				];
			}
		}
		return [
			'msg' => $msg ?? false,
			'listCat'=> $model->getAllCategories()
		];
	}
	

	public function changerTri(){
		$repoTri = new TriDefautRepository();

		if(isset($_POST['submitTri'])){
			$col = $_POST['colonne'] ?? null;
			$ordre = $_POST['ordre'] ?? null;

			if($col!==null && $ordre!==null){
				$colVal = ['nom', 'prix'];

				if(in_array($col, $colVal) && preg_match('#^[01]$#',$ordre)){
					try {
						$res = $repoTri->set($col,$ordre);
						if($res){
							$msg = [
								'type' => 'success',
								'msg'  => 'Modification effectuée'
							];
						}
						else {
							$msg = [
								'type' => 'info',
								'msg'  => 'Valeurs inchangées'
							];
						}
					} catch(Exception $e){
						$msg = [
							'type' => 'danger',
							'msg'  => $e->getMessage(),
						];
					}
				}
				else{
					$msg = [
						'type' => 'danger',
						'msg'  => "Champ invalide"
					];
				}
			}
			else {
				$msg = [
					'type' => 'danger',
					'msg'  => "Il manque des champs"
				];
			}

		}

		$tri = $repoTri->get(true);
		return [
			'curCol'   => $tri['colonne'],
			'curOrdre' => $tri['ordre'],
			'msg'      => $msg ?? null
		];
	}


	/**
	 * @param $inputName
	 * @return string
	 * @throws Exception
	 */
	private function processUploadedArticleImage($inputName){
		$imgPath       = $_FILES[$inputName]["name"];

		$imgInfos      = pathinfo($imgPath);
		$imgBasename   = $imgInfos['basename'];
		$imgName       = $imgInfos['filename'];
		$imgType       = strtolower($imgInfos['extension']);
		$imgSize       = $_FILES[$inputName]["size"];

		$imgPathServer = $_FILES[$inputName]["tmp_name"];

		$imgDirPath 	= ROOT.'public/img/articles/';

		if(getimagesize($imgPathServer) === false){
			throw new Exception("Le fichier n'est pas une image");
		}

		if ($imgSize > 500000) {
			throw new Exception("L'image est trop volumineuse (taille max : 1Mo)");
		}

		if($imgType != "jpg" && $imgType != "png" && $imgType != "jpeg" && $imgType != "gif" ) {
			throw new Exception("Seules les images PNG, JPG ou GIF sont autorisé");
		}

		$imgNewName = $imgBasename;
		$i=0;
		while(file_exists($imgDirPath.$imgNewName)){
			$imgNewName = $imgName.$i.$imgType;
			$i++;
		}

		if (move_uploaded_file($imgPathServer, $imgDirPath.$imgNewName)) {
			return $imgNewName;
		} else {
			throw new Exception("Erreur lors du l'enregistrement de l'image");
		}

	}
}
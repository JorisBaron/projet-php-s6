<?php


namespace App\Controller;


use App\Lib\Helper;
use App\Repository\ArticlesRepository;
use App\Repository\CommandesRepository;

class EspacePersoController extends AbstractController {

	public function __construct($app) {
		parent::__construct($app);
		if(!$this->app->user->isClient()){
			Helper::redirect(URL);
		}
	}

	public function index() {
		$this->app->view = 'espace-perso/commandes';
		return $this->commandes();
	}

	public function commandes(){
		$repoCommandes = new CommandesRepository();
		$repoArticles = new ArticlesRepository();

		return [
			'commandes' => $repoCommandes->getAllFromUser($this->app->user->getId()),
			'articles' 	=> $repoArticles->getAll()
		];
	}
}
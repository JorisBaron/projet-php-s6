<?php


namespace App\Controller;


use App\Lib\Helper;
use App\Renderer\JsonRenderer;
use App\Repository\CommandesRepository;
use Exception;
use PDOException;

class PaiementController extends AbstractController {

	public function __construct($app) {
		parent::__construct($app);
		if(!$this->app->user->isClient()){
			Helper::redirect(URL);
		}
	}

	public function index() {

		$repoCmd = new CommandesRepository();
		$waitingCmd = $repoCmd->getWaiting($this->app->user->getId());

		if(!$waitingCmd){
			Helper::redirect(URL);
		}

		if(isset($_POST['submitPaiement'])){
			$cb = ($_POST['numCarte1']??'') . ($_POST['numCarte2']??'') . ($_POST['numCarte3']??'') . ($_POST['numCarte4']??'');
			$dateExp = ($_POST['annee']??'') . '-' . ($_POST['mois']??'');
			$crypto = $_POST['crypto']??'';

			if(preg_match('#^[0-9]{16}$#',$cb)){
				if(Helper::validDate($dateExp, 'Y-m') && strtotime(date('Y-m')) <= strtotime($dateExp)){
					if(preg_match('#^[0-9]{3}$#',$crypto)){
						try {
							$repoCmd->confirmation($this->app->user->getId());
							$_SESSION['confCmd'] = true;
							Helper::redirect(URL.'paiement/confirmation');
						} catch(PDOException $e) {
							$msg = [
								'msg' => "Erreur lors de la confirmation de la commande",
								'type' => 'danger'
							];
						} catch(Exception $e){
							$msg = [
								'msg' => $e->getMessage(),
								'type' => 'danger'
							];
						}
					}
					else{
						$msg = [
							'msg' => "Cryptogramme invalide",
							'type' => 'danger'
						];
					}
				}
				else {
					$msg = [
						'msg' => "Date d'expiration invalide ou dépassée",
						'type' => 'danger'
					];
				}
			}
			else{
				$msg = [
					'msg' => "Numéro de carte invalide",
					'type' => 'danger'
				];
			}
		}

		$sommeTotale = array_reduce($waitingCmd, function ($somme, $item){
			return $somme + ((double)$item['prix'] * $item['qteCmd']);
		});


		return [
			'sommeTotale' => $sommeTotale,
			'msg' => $msg ?? false
		];
	}

	public function confirmation() {
		if(!isset($_SESSION['confCmd'])){
			Helper::redirect(URL);
		}
		unset($_SESSION['confCmd']);
	}

	public function ajaxAnnulation(){
		$this->app->setRenderer(new JsonRenderer());
		$repoCmd = new CommandesRepository();

		try{
			$repoCmd->annulation($this->app->user->getId());
		}catch(PDOException $e){
			$msg = [
				'msg' => $e->getMessage(),
				'type' => 'danger'
			];
		} catch(Exception $e){
			$msg = [
				'msg' => $e->getMessage(),
				'type' => 'danger'
			];
		}

		return [
			'err' => $msg ?? false
		];
	}
}
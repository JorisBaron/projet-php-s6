<?php

namespace App\Controller;
use App\Lib\Helper;
use App\Model\PanierModel;
use App\Model\ArticlesModel;
use App\Renderer\JsonRenderer;


class PanierController extends AbstractController {

    public function __construct($app)
    {
        parent::__construct($app);
        if ($this->app->user->isAdmin()) Helper::redirect(URL);
    }

    public function index() {
        $total = 0;
        if ($this->app->user->isGuest()) {
            if ($_SESSION["panier"]) $panier = $_SESSION["panier"];
            else $panier = [];
            foreach ($panier as $key => $value) {
                $total += $panier[$key]['prix'] * $panier[$key]['qte'];
            }
        }
        else {
            $model = PanierModel::getModel();
            $panier = $model->getUserPanier($this->app->user->getId());
            foreach ($panier as $key => $value) {
                $total += $panier[$key]['prix'] * $panier[$key]['qte'];
            }
        }
        return [
            "panier" => $panier,
            "total" => $total
        ];
    }

    public function ajouter () {
        $this->app->setRenderer(new JsonRenderer());
        $articleModel = ArticlesModel::getModel();
        $panierModel = PanierModel::getModel();
        if (empty($_GET) || empty($_GET['idArticle'])) {
            return [
                "err" => "Oops ! Il semblerait qu'il y ai eu un petit probleme",
                "panier" => ""
            ];
        }
        $article = $articleModel->getArticlesById($_GET['idArticle']);
        $article['qteArticle'] = $article['qte'];
        $article['qte'] = 1;
        if ($article['qteArticle']<=0) {
            return [
                "err" => "Oops ! Il semblerait qu'il n'y ai plus d'article disponible.",
                "panier" => $article
            ];
        }

        if ($this->app->user->isGuest()) {
            if (empty($_SESSION["panier"])) $_SESSION["panier"] = [];
            foreach ($_SESSION["panier"] as $key => $art) {
                if ($art['idArticle'] == $_GET['idArticle']) {
                    if ($art['qte'] < $art['qteArticle']) {
                        $_SESSION["panier"][$key]['qte']++;
                        $article['qte'] = $_SESSION["panier"][$key]['qte'];
                    }
                    else {
                        return [
                            "err" => "Oops ! Vous avez déjà tous les exemplaires de ce produit dans votre panier",
                            "panier" => $_SESSION["panier"][$key]
                        ];
                    }
                }
            }
            if ($article['qte'] === 1) array_push($_SESSION["panier"], $article);
            return [
                "panier" => $article,
                "err" => ""
            ];
        }
        else {
            $panier = $panierModel->getUserPanier($this->app->user->getId());
            foreach ($panier as $key => $art) {
                if ($art['idArticle'] == $_GET['idArticle']) {
                    if ($art['qte'] >= $art['qteArticle']) {
                        return [
                            "err" => "Oops ! Vous avez déjà tous les exemplaires de ce produit dans votre panier",
                            "panier" => $article
                        ];
                    }
                    $article['qte'] = $panier[$key]['qte']+1;
                }
            }
            if ($article['qte'] > 1) {
                $res = $panierModel->updatePanierQte((int)$this->app->user->getId(), (int)$_GET['idArticle'], $article['qte']);
            }
            else {
                $res = $panierModel->addPanier((int)$this->app->user->getId(), (int)$_GET['idArticle'], $article['qte']);
            }
            return [
                "panier" => $article,
                "err" => ""
            ];
        }
    }

    public function modifierQte()
    {
        $this->app->setRenderer(new JsonRenderer());

        if (!empty($_POST)) {
            if ($this->app->user->isGuest()) {
                foreach ($_SESSION["panier"] as $key => $art) {
                    if ($art['idArticle'] == $_POST['idPanier'] && $_POST['qte'] < $art['qteArticle']) {
                        $_SESSION["panier"][$key]['qte'] = $_POST['qte'];
                        $res = $_SESSION["panier"][$key];
                    }
                }
            }
            else {
                $model = PanierModel::getModel();
                $res = $model->updatePanierQte((int)$this->app->user->getId(), (int)$_POST['idPanier'], (int)$_POST['qte']);
            }
            return [
                'res' => $res
            ];
        }
    }

    public function supprimerPanier()
    {
        $this->app->setRenderer(new JsonRenderer());
        if ($this->app->user->isGuest()) {
            foreach ($_SESSION["panier"] as $key => $art) {
                if ($art['idArticle'] == $_POST['idPanier']) {
                    $article = $art;
                    unset($_SESSION["panier"][$key]);
                }
            }
            return [
                "res" => $article,
                "err" => ""
            ];
        }
        else {
            if (!empty($_POST)) {
                $model = PanierModel::getModel();
                $res = $model->deletePanier((int)$this->app->user->getId(), (int)$_POST['idPanier']);
            }
            return [
                "res" => $res,
                "err" => ""
            ];
        }
    }

    public function validerPanier()
    {
        $this->app->setRenderer(new JsonRenderer());
        $panierModel = PanierModel::getModel();
        $articleOutOfStock = [];
        $panier = $panierModel->getUserPanier($this->app->user->getId());
        foreach ($panier as $key => $art) {
            if ($art['qteArticle'] < $art['qte']) {
                $panierModel->updatePanierQte((int)$this->app->user->getId(), (int)$art['idArticle'], (int)$art['qteArticle']);
                array_push($articleOutOfStock, $art);
            }
        }
        return [
            "res" => $articleOutOfStock,
            "err" => ""
        ];
    }

    public function commanderPanier()
    {
        $this->app->setRenderer(new JsonRenderer());
        $panierModel = PanierModel::getModel();
        $panierModel->validerPanier((int)$this->app->user->getId());
        $idCommande = $panierModel->creerCommande((int)$this->app->user->getId());
        $panier = $panierModel->getUserPanier($this->app->user->getId());
        foreach ($panier as $key => $art) {
            $panierModel->panierToCommande($idCommande, (int)$art['idArticle'], (int)$art['qte']);
        }
    }
}
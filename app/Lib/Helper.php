<?php

namespace App\Lib;

use DateTime;

class Helper {

	/**
	 * Redirects to specified URL
	 * @param string $url
	 * @param int $statusCode
	 */
	static public function redirect(string $url, $statusCode = 303) {
		header('Location: '.$url, true, $statusCode);
		die();
	}

	public static function e($string){
		return htmlspecialchars($string, ENT_QUOTES);
	}

	public static function validDate($date, $format = 'Y-m-d'){
		$d = DateTime::createFromFormat($format, $date);
		// The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
		return $d && $d->format($format) === $date;
	}
}
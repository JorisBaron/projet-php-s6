<?php


namespace App\Renderer;


use App\Core\Application;

interface RendererInterface {
	public function render(Application $app);
}
<?php


namespace App\Renderer;


use App\Core\Application;

class DefaultRenderer implements RendererInterface {

	public function render(Application $app) {
		$view = $app->view;

		$data = $app->data;
		extract($data);

		require APP.'views/_templates/header.phtml';
		if(file_exists(APP.'views/'.$view.".phtml")) {
			require APP.'views/'.$view.".phtml";
		} else {
			echo '<main class="container"><h1 class="display-1 lead">Vue inexistante</h1></main>';
		}
		require APP.'views/_templates/footer.phtml';
	}
}
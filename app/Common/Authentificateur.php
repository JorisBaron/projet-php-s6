<?php


namespace App\Common;

use App\Model\Model;

/**
 * Class Authentificateur
 * @package App\Common
 */
class Authentificateur {

    /**
     * Authentificateur constructor.
     */
    public function __construct() {
		if(session_status() == PHP_SESSION_NONE) {
			session_set_cookie_params(['path'=> URL_SUB_FOLDER, 'httponly'=>true]);
			session_start();
		}
	}

	public function login($id) {
        $_SESSION['id'] = $id;
    }

    public function logout(){
        unset($_SESSION['id']);
    }

    public function isLogged(){
        return isset($_SESSION['id']);
    }

    public function getCurrentUser(){
        return $_SESSION['id'] ?? false;
    }
}
<?php

namespace App\Common;

/**
 * Classe User
 * @package App\Common
 */
class User
{
    /**
     * Attribut de la classe User
     */
    private $idUser;
    private $email;
    private $nom;
    private $prenom;
    private $mdp;
    private $admin;

    /**
     * Constructeur de l'utilisateur.
     * @param $array_data
     */
   public function __construct($array_data)
    {
        if($array_data) {
            $this->idUser = (int)$array_data["idUser"];
			$this->email  = $array_data["email"];
			$this->nom    = $array_data["nom"];
			$this->prenom = $array_data["prenom"];
			$this->mdp    = $array_data["mdp"];
            $this->admin  = (int)$array_data["admin"];
        } else {
            $this->idUser = "";
            $this->email  = "";
            $this->nom    = "";
            $this->prenom = "";
            $this->mdp    = "";
            $this->admin  = 2;
        }
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->idUser;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * @return string
     */
    public function getMdp() {
        return $this->mdp;
    }

    public function getRole() {
        return $this->admin;
    }

    /**
     * @return bool
     */
    public function isAdmin() {
        return $this->admin == 1;
    }

    /**
     * @return bool
     */
    public function isGuest() {
        return $this->admin == 2;
    }

    /**
     * @return bool
     */
    public function isClient() {
        return $this->admin == 0;
    }
}
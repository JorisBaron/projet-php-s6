<?php


namespace App\Repository;


class ArticlesRepository extends AbstractRepository {
	public function getAll(){
		$req = $this->db->prepare("SELECT * FROM `articles`");
		$req->execute();

		$rawRes = $req->fetchAll();
		$res = [];
		foreach($rawRes as $article){
			$id = $article['idArticle'];
			unset($article['idArticle']);
			$res[$id] = $article;
		}

		return $res;
	}
}
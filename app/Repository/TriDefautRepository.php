<?php


namespace App\Repository;


use Exception;

class TriDefautRepository extends AbstractRepository {
	const TRI_CROISSANT   = 1;
	const TRI_DECROISSANT = 0;
	const TRI_NOM  = 'nom';
	const TRI_PRIX = 'prix';

	public function get($raw=false){
		if($raw)
			$colOrdre = "`croissant`";
		else {
			$colOrdre = "IF(`croissant` = 1, 'ASC', 'DESC')";
		}
		$req = $this->db->prepare("SELECT `colonne`, ".$colOrdre." as `ordre` FROM `tri_par_defaut`;");
		$req->execute();
		return $req->fetch();
	}

	/**
	 * @param $colonne
	 * @param $ordre
	 * @return bool
	 * @throws Exception
	 */
	public function set($colonne, $ordre){
		$req = $this->db->prepare("UPDATE `tri_par_defaut` SET `colonne`=:c, `croissant`=:o;");
		$req->bindValue(':c', $colonne);
		$req->bindValue(':o', $ordre);

		if(!$req->execute())
			throw new Exception("Erreur lors de la modification du tri par défaut");
		else
			return $req->rowCount()==1;
	}
}
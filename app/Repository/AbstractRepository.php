<?php


namespace App\Repository;


use App\Core\DatabaseConnexion;
use PDO;

abstract class AbstractRepository {
	/** @var PDO */
	protected $db;

	public function __construct() {
		$this->db = DatabaseConnexion::getDb();
	}

}
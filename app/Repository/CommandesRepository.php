<?php


namespace App\Repository;


use Exception;
use PDO;
use PDOException;

class CommandesRepository extends AbstractRepository {
	public function getAllFromUser($idUser){
		$sql = "SELECT `idCommande`,`idUser`, DATE_FORMAT(`dateCmd`,'%d/%m/%Y') as `date`, GROUP_CONCAT(CONCAT(`idArticle`,',',`qteCmd`) SEPARATOR ';') as `articles`
				FROM `commandes` `c` 
				    NATURAL JOIN `commandes_articles` `ca`
				WHERE `idUser` = :id
				GROUP BY `idCommande`
				ORDER BY `dateCmd` DESC;";
		$req = $this->db->prepare($sql);
		$req->bindValue(':id', $idUser, PDO::PARAM_INT);
		$req->execute();

		$res = $req->fetchAll();

		array_walk($res, function (&$commande){
			$articles = explode(';',$commande['articles']);
			array_walk($articles, function (&$articles){
				$article = explode(',', $articles);
				$articles = [
					'idArticle' => (int)$article[0],
					'qte'       => (int)$article[1]
				];
			});
			$commande['articles'] = $articles;
		});

		return $res;
	}

	public function getWaiting($idUser){
		$req = $this->db->prepare(
			"SELECT `c`.`idCommande`, `a`.`nom`, `a`.`prix`, `ca`.`qteCmd`, `a`.`img`, `a`.`idArticle`, `a`.`qte` AS qteArticle 
			FROM `commandes` AS `c`
			    JOIN `commandes_articles` `ca` ON `c`.`idCommande` = `ca`.`idCommande`
				JOIN `articles` `a` ON `ca`.`idArticle` = `a`.`idArticle`
			WHERE `c`.`idUser`= :id AND `c`.`etat`=0;");
		$req->bindValue(':id', $idUser);
		$req->execute();

		return $req->fetchAll();
	}

	public function confirmation($idUser){
		try {
			$this->db->beginTransaction();

			$reqConfCmd = $this->db->prepare(
				"UPDATE `commandes` SET `etat` = 1 WHERE `etat` = 0 AND `idUser`=:id;"
			);
			$reqConfCmd->bindValue(':id',$idUser);
			$reqConfCmd->execute();

			if($reqConfCmd->rowCount()==0) {
				throw new Exception("Aucune commande en attente");
			}

			$reqClearPanier = $this->db->prepare(
				"DELETE FROM `paniers` WHERE `idUser` = :id");
			$reqClearPanier->bindValue(':id', $idUser);
			$reqClearPanier->execute();

			$this->db->commit();
		} catch(PDOException | Exception $e){
			$this->db->rollBack();
			throw $e;
		}
	}

	public function annulation($idUser){
		try {
			$this->db->beginTransaction();

			$reqCmdAttente = $this->db->prepare(
				"SELECT `idCommande` FROM `commandes` 
				WHERE `idUser`=:id AND `etat`=0;"
			);
			$reqCmdAttente->bindValue(':id',$idUser);
			$reqCmdAttente->execute();
			$idCmd = $reqCmdAttente->fetch()['idCommande'];

			if(!$idCmd){
				throw new Exception("Aucune commande en attente");
			}

			$reqRestock = $this->db->prepare(
				"UPDATE `articles` `a` JOIN `commandes_articles` `ca` ON `a`.`idArticle` = `ca`.`idArticle` AND `ca`.`idCommande`=:idCmd
				SET a.`qte` = a.`qte` + `ca`.`qteCmd`;"
			);
			$reqRestock->bindValue(':idCmd',$idCmd);
			$reqRestock->execute();


			$reqDeleteCmdArticle = $this->db->prepare(
				"DELETE FROM `commandes_articles` WHERE `idCommande` = :idCmd"
			);
			$reqDeleteCmdArticle->bindValue(':idCmd', $idCmd);
			$reqDeleteCmdArticle->execute();

			$reqDeleteCmd = $this->db->prepare(
				"DELETE FROM `commandes` WHERE `idCommande` = :idCmd"
			);
			$reqDeleteCmd->bindValue(':idCmd', $idCmd);
			$reqDeleteCmd->execute();

			$this->db->commit();
		} catch(PDOException | Exception $e){
			$this->db->rollBack();
			throw $e;
		}
	}

	public function hasWaiting($idUser){
		$reqCmdAttente = $this->db->prepare("SELECT * FROM `commandes` WHERE `idUser`=:id AND `etat`=0;");
		$reqCmdAttente->bindValue(':id',$idUser);
		$reqCmdAttente->execute();
		return $reqCmdAttente->fetch()!==false;
	}
}
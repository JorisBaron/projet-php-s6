<?php


namespace App\Repository;


use App\Common\User;
use Exception;

class UserRepository extends AbstractRepository {

	public function add($email, $nom, $prenom, $mdp) {
		$req = $this->db->prepare("INSERT into users (email, nom, prenom,mdp) VALUES (:em, :en, :pr,:md);");
		$req->bindValue('em', $email);
		$req->bindValue('en', $nom);
		$req->bindValue('pr', $prenom);
		$req->bindValue('md', password_hash($mdp, PASSWORD_DEFAULT));
		$req->execute();

		if($req->rowCount()!=1)
			throw new Exception("Échec de l'enregistrement de l'utilisateur");
	}

	public function getByEmail($email) {
		$req = $this->db->prepare("SELECT * FROM users WHERE email= :email;");
		$req->bindValue(':email', $email);
		$req->execute();

		return new User($req->fetch());
	}

	public function getById($id) {
		$req = $this->db->prepare("SELECT * FROM users WHERE `idUser`= :id;");
		$req->bindValue(':id', $id);
		$req->execute();

		return new User($req->fetch());
	}

	public function verifyPassword($email, $password):bool {
		$req = $this->db->prepare("SELECT mdp FROM users WHERE email= :email;");
		$req->bindValue(':email', $email);
		$req->execute();
		$user = $req->fetch();

		return $user !==false && password_verify($password, $user["mdp"]);
	}
}
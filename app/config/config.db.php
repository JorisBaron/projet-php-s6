<?php
return [
	'driver' => 'mysql',
	'charset' => 'utf8mb4',
	'host' => 'localhost',
	'port' => '3306',
	'name' => 'galilee_web_s6_projet',
	'user' => 'root',
	'pass' => '',
	'default_options' => [
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_EMULATE_PREPARES   => false,
	],
];
<?php

/*
 * Exemple pour ajouter un item au menu :
 *
 * Lien :
[
	'type'  => 'link',
	'label' => 'abcd',
	'route' => [
		'controller' => '123',
		'action'     => 'abc'
	],
	'link' => '123/abc'
],

 *
 * Sous-menu :
[
	'type' => 'submenu',
	'label' => 'abcd',
	'submenu' => [
		[
			//items
		],
	]
],

 *
 * Séparateur (dans un sous-menu seulement) :
[
	'type' => 'separator',
],

 */

return [
	'default' => [
		'left' => [

		],
		'right' => [

		]
	],
	'admin' => [
		'left' => [

		],
		'right' => [

		]
	],
	'guest' => [
		'left' => [

		],
		'right' => [

		]
	]
];
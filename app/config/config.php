<?php

return[
	'app' => [
		'name' => 'AIRmazon'
	],

	'db' => require_once 'config.db.php',

	'menubar' => require_once 'config.menubar.php',

	'default_controller' => 'index',
	'default_action' => 'index',

	'maxImageSize' => 1048576, // 1Mo
];
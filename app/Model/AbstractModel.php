<?php

namespace App\Model;

use PDO;
use PDOException;

abstract class AbstractModel
{
	/** @var array[Model] Instances uniques des Model enfants */
	private static $instances = [];

	/** @var PDO instance PDO */
	protected $db;


	/**
	 * Model constructor.
	 * Design pattern Singleton
	 */
	protected function __construct() {
		try {
			$configs = CONFIG['db'];

			$dns = $configs['driver'].':host='.$configs['host'].';port='.$configs['port'].';dbname='.$configs['name'].";charset=".$configs['charset'];;
			$options = $configs['default_options'];

			$this->db = new PDO($dns, $configs['user'], $configs['pass'], $options);
		}
		catch(PDOException $e){
			die('Connexion à la base de données impossible.<br> Erreur : '. $e->getMessage());
		}
	}

	public static function getModel(){
		if(!isset(self::$instances[static::class])){
			self::$instances[static::class] = new static();
		}
		return self::$instances[static::class];
	}

}

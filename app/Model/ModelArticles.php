<?php


namespace App\Model;




use Exception;

/**
 * Class Model
 * @package App\Model
 * @method static ModelArticles getModel()
 */
class ModelArticles extends AbstractModel {
    
    public function addArticles($nom,$qte,$cat,$prix)
    {
        $req = $this->db->prepare("INSERT into articles (nom, qte, idCat,prix) VALUES (:em, :en, :pr,:md);");
        $req->bindValue('em', $nom);
        $req->bindValue('en', $qte);
        $req->bindValue('pr', $cat);
        $req->bindValue('md', $prix);
        $req->execute();
        return $req->rowCount()==1;

    }

	/**
	 * @param $nom
	 * @param $qte
	 * @param $cat
	 * @param $prix
	 * @param null $img
	 * @throws Exception
	 */
    public function addArticle($nom,$qte,$cat,$prix,$img = null)
    {
        $req = $this->db->prepare("INSERT into articles (nom, qte, idCat,prix,img) VALUES (:nom, :qte, :cat, :prix, :img);");
        $req->bindValue('nom', $nom);
        $req->bindValue('qte', $qte);
        $req->bindValue('cat', $cat);
        $req->bindValue('prix', $prix);
        $req->bindValue('img', $img);
        if(!($req->execute() && $req->rowCount()==1))
        	throw new Exception("Erreur lors de l'ajout de l'article");

    }

    public function getAllCategories(){
        $req = $this->db->prepare("SELECT * FROM categories ; ");
        $req->execute();
        return $req->fetchAll();
        
    }



}
<?php


namespace App\Model;




use PDO;

/**
 * Class Model
 * @package App\Model
 * @method static PanierModel getModel()
 */
class PanierModel extends AbstractModel {

    public function getUserPanier ($id) {

        $req = $this->db->prepare("SELECT a.nom, a.prix, p.qte, a.img, a.idArticle, a.qte AS qteArticle FROM paniers AS p LEFT JOIN articles AS a ON p.idArticle=a.idArticle WHERE p.idUser= :id;");
        $req->bindValue(':id', $id);
        $req->execute();
        $result= $req->fetchAll();
        return $result;
    }

    public function updatePanierQte ($idUser, $idArticle, $newQte) {

        $req = $this->db->prepare("UPDATE paniers SET qte=:newQte WHERE idArticle=:idArticle AND idUser=:idUser;");
        $req->bindValue(':newQte', $newQte);
        $req->bindValue(':idArticle', $idArticle);
        $req->bindValue(':idUser', $idUser);
        $req->execute();
        return $req;
    }

    public function addPanier ($idUser, $idArticle, $qte) {
        $req = $this->db->prepare("INSERT INTO paniers (idUser, idArticle, qte) VALUES (:idUser, :idArticle, :qte);");
        $req->bindValue(':idUser', $idUser);
        $req->bindValue(':idArticle', $idArticle);
        $req->bindValue(':qte', $qte);
        $req->execute();
        $result= $req->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function deletePanier ($idUser, $idArticle) {
        $req = $this->db->prepare("DELETE FROM paniers WHERE idArticle=:idArticle AND idUser=:idUser;");
        $req->bindValue(':idUser', $idUser);
        $req->bindValue(':idArticle', $idArticle);
        $req->execute();
        $result= $req->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function validerPanier ($idUser) {
        $req = $this->db->prepare("UPDATE articles a JOIN paniers p ON a.idArticle = p.idArticle AND p.idUser=:idUser SET a.qte = a.qte - p.qte;");
        $req->bindValue(':idUser', $idUser);
        $req->execute();
        return $req;
    }

    public function creerCommande ($idUser) {
        $req = $this->db->prepare("INSERT INTO commandes (idUser) VALUES (:idUser);");
        $req->bindValue(':idUser', $idUser);
        $req->execute();
        return $this->db->lastInsertId();
    }

    public function panierToCommande ($idCommande, $idArticle, $qteCmd) {
        $req = $this->db->prepare("INSERT INTO commandes_articles (idCommande, idArticle, qteCmd) VALUES (:idCommande, :idArticle, :qteCmd);");
        $req->bindValue(':idCommande', $idCommande);
        $req->bindValue(':idArticle', $idArticle);
        $req->bindValue(':qteCmd', $qteCmd);
        $req->execute();
        return $req;
    }
}
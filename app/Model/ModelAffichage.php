<?php


namespace App\Model;




use App\Repository\TriDefautRepository;
use PDO;

/**
 * Class Model
 * @package App\Model
 * @method static ModelAffichage getModel()
 */
class ModelAffichage extends AbstractModel {
    # récupérer la liste des articles
    public function getArticles()
    {
        $req = $this->db->prepare("SELECT * FROM `articles`;");
        $req->execute();
        return $req->fetchAll();
    }


    // récupérer toutes les catégories
    public function getAllCategories(){
        $req = $this->db->prepare("SELECT * FROM `categories`;");
        $req->execute();
        return array_column($req->fetchAll(),'nom','idCat');
    }

    public function getArticlesFiltered($cat=[], $min=null, $max=null, $colTri=null, $ordre=null){
        // requete initiale
        $request = "SELECT * FROM `articles`";
        $array_param = array();

        // construction des parametres
        if(!empty($cat)) {
            $paramCat = "idCat in (:cat0";
            for($i = 1 ; $i<count($cat) ; $i++){
                $paramCat .= ", :cat" . $i;
            }
            $paramCat .= ")";
            $array_param[] = $paramCat;
        }

        if($min !== null) {
            $array_param[] = "prix >= :min";
        }

        if($max !== null) {
            $array_param[] = "prix <= :max";
        }

        //param tri
		if($colTri!==null && $ordre!==null){
			$tri = ' ORDER BY `'.$colTri.'` ' . ($ordre==TriDefautRepository::TRI_CROISSANT ? 'ASC' : 'DESC');
		}
		else{
			$repoTri = new TriDefautRepository();
			$triDefaut = $repoTri->get();

			$tri = ' ORDER BY `'.$triDefaut['colonne'].'` ' . $triDefaut['ordre'];
		}

        // concatenation
        if(!empty($array_param)) {
            $request .= " WHERE " . implode(" AND ", $array_param);
        }

        $request .= $tri /*. " LIMIT " . ($page-1)*10 . ',10;'*/;

        $query = $this->db->prepare($request);
        if(!empty($cat)) {
            for($i=0 ; $i<count($cat) ; $i++){
                $query->bindValue(":cat" . $i, $cat[$i]);
            }
        }

        if($min !== null) {
            $query->bindValue(":min", $min);
        }

        if($max !== null) {
            $query->bindValue(":max", $max);
        }

        $query->execute();
        return $query->fetchAll();
    }
}
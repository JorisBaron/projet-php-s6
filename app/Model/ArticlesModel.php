<?php


namespace App\Model;



/**
 * Class Model
 * @package App\Model
 * @method static ArticlesModel getModel()
 */
class ArticlesModel extends AbstractModel {
    # récupérer la liste des articles
    public function getArticlesById($id)
    {
        $req = $this->db->prepare("SELECT * FROM articles WHERE idArticle=:id;");
        $req->bindValue(':id', $id);
        $req->execute();
        return $req->fetch();
    }
}
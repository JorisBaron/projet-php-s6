# Projet de Programmation Web 

# Site de e-commerce : AIRmazon

## Description

Ceci est une plateforme e-commerce qui propose la vente d'articles de différentes catégories (jeux-vidéos, livre, musique, peluche, fourniture de bureau et high-tech).

L'application sert à mettre en pratique les notions acquises autour du PHP de base (session, cookies, base de données) ainsi que de l'AJAX et du CSS.

## Réalisation du projet
 * Joris BARON
 * Maurane GLAUDE  
 * Tinhinane KADRI
 * Vanelle WOUMFO KENFACK
 * Ali ZAMAN

## Environnements de développement
 * WampServer 3.2 (Windows)
 * PHP 7.3
 * MariaDB version 10.4

⚠ Ne pas utiliser IE pour exécuter l'application ⚠

## Importation de la base de données
Importer les fichiers `db_struct.sql` puis `db_data.sql` dans votre base de données (dossier `data`).

## Comptes existants dans la base de données
Comptes Administrateurs:
 * Email : `jooj@jooj.io`  
   MDP : `Joris`
 * Email : `ali@mail.fr`   
   MDP : `Ali`
 * Email : `maurane@mail.fr`  
   MDP : `Maurane`
 * Email : `vanelle@mail.fr`  
   MDP : `Vanelle`
 * Email : `tinhinane@mail.fr`  
   MDP : `Tinhinane`

Comptes Utilisateurs:
 * Email: `toto@mail.fr`  
   MDP: `Toto`
 * Email: `tata@mail.fr`  
   MDP: `Tata`


# Bonne navigation 🙂
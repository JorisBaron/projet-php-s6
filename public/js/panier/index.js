$(document).ready(function() {
    $("input.quantitee").on("blur", function(e) {
        e.preventDefault();

        $.post(
            URL + "panier/modifierQte",
            {
                'qte': $(this).val(),
                'idPanier': $(this).parent().parent()[0].id
            },
            function (data) {
                console.log("data: ", data);
            }
        );
    });

    $("input.quantitee").on("change", function(e) {
        e.preventDefault();
        if (parseInt($(this).val()) > parseInt($(this)[0].max)) $(this)[0].value = $(this)[0].max;
        if (parseInt($(this).val()) < parseInt($(this)[0].min)) $(this)[0].value = 1;

        $(this).parent().parent()[0].children[4].innerHTML = (parseFloat($(this).parent().parent()[0].children[2].innerText) * $(this).val()).toFixed(2) + "€";
        prixTotal = 0.0;
        nbrArticle = $(this).parent().parent().parent().children().length - 1;
        for (let i = 0; i < nbrArticle; i++) {
            if ($(this).parent().parent().parent().children()[i].children[4].innerText) prixTotal += parseFloat($(this).parent().parent().parent().children()[i].children[4].innerText);
        }

        $(this).parent().parent().parent().children()[nbrArticle].children[4].innerText = prixTotal.toFixed(2) + "€";
        $(this).focus();
    });

    $("button.delete").on("click", async function (e) {
        e.preventDefault();
        test = await $.post(
            URL + "panier/supprimerPanier",
            {
                'idPanier': $(this).parent().parent()[0].id
            });
        prixProduit = parseFloat(document.getElementById($(this).parent().parent()[0].id).children[4].innerText);
        PrixTotal = parseFloat(document.getElementById("prixTotal").innerText);

        document.getElementById("prixTotal").innerText = (PrixTotal - prixProduit).toFixed(2) + "€";
        document.getElementById("panierTable").children[1].removeChild($(this).parent().parent()[0]);
    });

    $("#validationPanierBtn").on("click", function (e) {
        e.preventDefault();

        $.get(
            URL + "panier/validerPanier",
            function (data) {
                if (data.res.length == 0) {
                    document.getElementById("validationModalText").innerText = "Votre panier à été validé, vous pouvez maintenant procéder au payment";
                }
                else {
                    text = "<div class='text-dark text-left'><p>Vous avez sélectionné certains produit en trop grande quantité</p>";
                    data.res.forEach(article => {
                        text += "<p>- "+article["nom"]+"</p>&emsp; - Sélectionné: <span class='text-primary'>"+article["qte"]+"</span><br/>&emsp; - Disponible: <span class='text-primary'>"+article["qteArticle"]+"</span><br/><br/>";
                    })
                    text += "</div>";
                    document.getElementById("validationModalText").innerHTML = text;
                }
                document.getElementById("passerAuPayementBtn").hidden = false;
            }
        );
    });

    $("#passerAuPayementBtn").on("click", function (e) {
        e.preventDefault();
        $.get(
            URL + "panier/commanderPanier",
            function() {
                window.location.replace(URL+"paiement");
            });
    });
});
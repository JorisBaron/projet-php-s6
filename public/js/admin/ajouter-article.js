$(document).ready(function() {
	const imageInput = $('#imageInput');

	imageInput.change(function() {
		$('label[for="'+$(this).attr('id')+'"].custom-file-label').text(
			$(this).val().split(/[\\/]/).pop()
		);
	})
})
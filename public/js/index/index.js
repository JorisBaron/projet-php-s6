$(document).ready(function() {
	//ajout panier
	$("#listeArticle button").on("click", function(e) {
		e.preventDefault();

		$.get(
			URL + "panier/ajouter?idArticle="+$(this).parent().parent().parent().attr('data-article-id'),
			function (data) {
				if (data.err !== "") {
					$("#alertTitre").text("Erreur");
					$("#alertText").text(data.err);
				}
				else {
					$("#alertTitre").text("Article ajouté à votre panier");
					$("#alertText").text("");
				}
				$("#alertArticleImage").attr("src", URL+"img/articles/"+data.panier.img);
				$("#alertArticleNom").text(data.panier.nom);
				$("#alertArticlePrix").text(data.panier.prix + "€");

				$("#alert")[0].hidden=false;
				setTimeout(function(){ $("#alert")[0].hidden=true; }, 2500);
			}
		);
	});

	$('#pagination a.page-link').click(function(e) {

	})
})
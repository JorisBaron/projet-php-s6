$(document).ready(function() {
	const inputCB = $('.inputCB');
	const inputCbContainer = inputCB.eq(0).parent().parent();

	const inputMois  = $('#moisInput');
	const inputAnnee = $('#anneeInput');

	const inputCrypto = $('#cryptoInput');

	let formSubmit = false

	inputCB.on('keydown',function(e) {
		if(e.which === 8){
			if($(this).val().length===0)
				$(this).parent().prev().children().focus();
		}
		else if($(this).val().length===4){
			$(this).parent().next().children().focus();
		}
	});

	inputMois.on('keydown',function(e) {
		if(e.key === '/'){
			e.preventDefault();
		}
		if($(this).val().length===2 && !(e.which === 8 || e.which === 46 || e.which === 9)){
			inputAnnee.focus();
		}
	});

	inputAnnee.on('keydown',function(e) {
		if(e.which === 8){
			if($(this).val().length===0)
				inputMois.focus();
		}
	});

	$('#formPaiement').on('submit',function() {
		formSubmit=true;
	})

	$(window).on('unload', function() {
		if(!formSubmit) {
			$.post({
				url: URL + 'paiement/ajax-annulation',
				async: false
			});
		}
	})
})
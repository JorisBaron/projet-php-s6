$(document).ready(function() {
    const formConnexion = $("#formConnexion");

    const alertTemplate =
        $('<div id="error-div" class="alert alert-danger alert-dismissible" role="alert">\n' +
            '<span id="error-connexion-message"></span>\n' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                '<span aria-hidden="true">&times;</span>\n' +
            '</button>\n' +
        '</div>');

    formConnexion.on("submit", function(e) {
        e.preventDefault();

        const emailField = $("#email-connexion");
        const mdpField   = $("#mdp-connexion");

        const email = emailField.val().trim();
        const mdp   = mdpField.val().trim();

        const regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if(!regexEmail.test(email)){
            displayConnexionAlert("Email invalide");
            emailField.addClass('border-danger');
        }
        else if(mdp.length === 0){
            displayConnexionAlert("Mot de passe invalide");
            mdpField.addClass('border-danger');
        }
        else {
            emailField.removeClass('border-danger');
            mdpField.removeClass('border-danger');
            $.post(
                $(this).attr('action'),
                {
                    'email': email,
                    'mdp': mdp
                },
                function(data) {
                    if(data.error !== false) {
                        formConnexion.find('.alert').alert('close');
                        displayConnexionAlert(data.error);
                    } else {
                        window.location.reload();
                    }
                });
        }

    });


    function displayConnexionAlert(msg) {
        formConnexion.find('.alert').alert('close');
        const alert = alertTemplate.clone();
        alert.find('#error-connexion-message').text(msg);
        $("#connexion-body").prepend(alert);
    }
});

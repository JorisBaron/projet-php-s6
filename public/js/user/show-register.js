$(window).ready(function(){
    const formRegister = $('#formRegister');

    const emailElm  = $('#email');
    const nomElm    = $('#nom');
    const prenomElm = $('#prenom');
    const mdp1Elm   = $('#mdp1');
    const mdp2Elm   = $('#mdp2');

    const regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    const alertTemplate =
        $('<div id="error-div" class="alert alert-danger alert-dismissible" role="alert">\n' +
            '<span id="error-connexion-message"></span>\n' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
            '<span aria-hidden="true">&times;</span>\n' +
            '</button>\n' +
            '</div>');

    formRegister.on("submit", function(e){
        //on prévient de
        e.preventDefault()
        //


        formRegister.find('input').each(function() {
            $(this).removeClass('border-danger');
        })


        const email  = emailElm.val().trim();
        const nom    = nomElm.val().trim();
        const prenom = prenomElm.val().trim();
        const mdp1   = mdp1Elm.val().trim();
        const mdp2   = mdp2Elm.val().trim();

        console.log(email)

        if(!regexEmail.test(email) || email.length>255){
            displayAlert("Email invalide");
            emailElm.addClass('border-danger');
        }
        else if(nom.length === 0 || nom.length>255) {
            displayAlert("Nom invalide");
            nomElm.addClass('border-danger');
        }
        else if(prenom.length === 0 || prenom.length>255) {
            displayAlert("Prénom invalide");
            prenomElm.addClass('border-danger');
        }
        else if(mdp1.length === 0 || mdp1.length > 255){
            displayAlert("Mot de passe invalide");
            mdp1Elm.addClass('border-danger');
        }
        else if(mdp1 !== mdp2){
            displayAlert("Mot de passe différents");
            mdp1Elm.addClass('border-danger');
            mdp2Elm.addClass('border-danger');
        }
        else {
            const dataObj ={
                email  : email,
                nom    : nom,
                prenom : prenom,
                mdp1   : mdp1,
                mdp2   : mdp2,
            }

            console.log(dataObj)

            $.post(
                $(this).attr('action'),
                dataObj,
                function (data) {
                    // si le serveur retourne une erreur
                    if(data.error !== false) {
                        formRegister.find('.alert').alert('close');
                        displayAlert(data.error);
                    } else {
                        window.location.replace(URL);
                    }
                }
            );
        }
    });

    function displayAlert(msg) {
        formRegister.find('.alert').alert('close');
        const alert = alertTemplate.clone();
        alert.find('#error-connexion-message').text(msg);
        $("#inscription-body").prepend(alert);
    }
});
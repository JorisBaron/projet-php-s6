$(document).ready(function() {
	$('.collapse').on('show.bs.collapse', function() {
		const icons = $(this).prev().find('.collapse-icon');
		console.log(icons)
		icons.eq(0).addClass('d-none');
		icons.eq(1).removeClass('d-none');
	}).on('hide.bs.collapse', function() {
		const icons = $(this).prev().find('.collapse-icon');
		icons.eq(0).removeClass('d-none');
		icons.eq(1).addClass('d-none');
	})
})

<?php



define('DS',DIRECTORY_SEPARATOR);
define('ROOT', dirname(__DIR__) . DS);
define('APP', ROOT . 'app' . DS);

define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', rtrim(str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME'],2)), '/').'/');
define('URL', '//' . URL_DOMAIN . URL_SUB_FOLDER);

define('CONFIG', require APP . 'config'.DS.'config.php');

//autoload
require APP.'Core'.DS.'autoload.php';

// application
use App\Core\Application;
new Application();
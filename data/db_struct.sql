CREATE DATABASE IF NOT EXISTS `galilee_web_s6_projet` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `galilee_web_s6_projet`;

DROP TABLE IF EXISTS `commandes_articles`;
CREATE TABLE IF NOT EXISTS `commandes_articles` (
    `idCommande` int(11) NOT NULL,
    `idArticle` int(11) NOT NULL,
    `qteCmd` int(11) NOT NULL DEFAULT 1,
    PRIMARY KEY (`idCommande`,`idArticle`),
    KEY `fk-cmdArticle-article` (`idArticle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `paniers`;
CREATE TABLE IF NOT EXISTS `paniers` (
    `idUser` int(11) NOT NULL,
    `idArticle` int(11) NOT NULL,
    `qte` int(11) NOT NULL DEFAULT 1,
    PRIMARY KEY (`idUser`,`idArticle`),
    KEY `fk-paniers-articles` (`idArticle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
    `idArticle` int(11) NOT NULL AUTO_INCREMENT,
    `nom` varchar(255) NOT NULL,
    `idCat` int(11) NOT NULL,
    `prix` decimal(6,2) NOT NULL,
    `qte` int(11) NOT NULL,
    `img` varchar(255),
    PRIMARY KEY (`idArticle`),
    KEY `fk-articles-categories` (`idCat`),
    KEY `nom` (`nom`),
    KEY `prix` (`prix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
    `idCat` int(11) NOT NULL AUTO_INCREMENT,
    `nom` varchar(50) NOT NULL,
    PRIMARY KEY (`idCat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `commandes`;
CREATE TABLE IF NOT EXISTS `commandes` (
    `idCommande` int(11) NOT NULL AUTO_INCREMENT,
    `idUser` int(11) NOT NULL,
    `dateCmd` datetime NOT NULL DEFAULT current_timestamp(),
    `etat` TINYINT(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (`idCommande`),
    KEY `fk-cmd-users` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `tri_par_defaut`;
CREATE TABLE IF NOT EXISTS `tri_par_defaut` (
    `colonne` varchar(50) NOT NULL,
    `croissant` tinyint(1) NOT NULL,
    PRIMARY KEY (`colonne`,`croissant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
    `idUser` int(11) NOT NULL AUTO_INCREMENT,
    `email` varchar(255) NOT NULL,
    `nom` varchar(255) NOT NULL,
    `prenom` varchar(255) NOT NULL,
    `mdp` varchar(255) NOT NULL,
    `admin` TINYINT DEFAULT 0,
    PRIMARY KEY (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `articles`
    ADD CONSTRAINT `fk-articles-categories` FOREIGN KEY (`idCat`) REFERENCES `categories` (`idCat`) ON UPDATE CASCADE;

ALTER TABLE `commandes`
    ADD CONSTRAINT `fk-cmd-users` FOREIGN KEY (`idUser`) REFERENCES `users` (`idUser`) ON UPDATE CASCADE;

ALTER TABLE `commandes_articles`
    ADD CONSTRAINT `fk-cmdArticle-article` FOREIGN KEY (`idArticle`) REFERENCES `articles` (`idArticle`),
    ADD CONSTRAINT `fk-cmdArticle-cmd` FOREIGN KEY (`idCommande`) REFERENCES `commandes` (`idCommande`) ON UPDATE CASCADE;

ALTER TABLE `paniers`
    ADD CONSTRAINT `fk-paniers-articles` FOREIGN KEY (`idArticle`) REFERENCES `articles` (`idArticle`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk-paniers-users` FOREIGN KEY (`idUser`) REFERENCES `users` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE;

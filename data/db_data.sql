USE `galilee_web_s6_projet`;

INSERT INTO `users` (`idUser`, `email`, `nom`, `prenom`, `mdp`, `admin`)
VALUES (1, 'jooj@jooj.io'     , 'Baron' , 'Joris'    , '$2y$10$LU01B4tJLNpVHOo6stiRbOSUjziX2xikmRgiG97VXGCXAH/ocftc2', 1),
       (2, 'ali@mail.fr'      , 'Zaman' , 'Ali'      , '$2y$10$7QAsugbGcOrkPoVsyQ.7JuaOGabXc4M.m2Vfz.kXXLFkSLtHM5DyO', 1),
       (3, 'maurane@mail.fr'  , 'Glaude', 'Maurane'  , '$2y$10$3rIEn.ktPmJ0.CGvVCPCsuV2.fhTk.taVaGXl6ZWwBGDO9/r97eu.', 1),
       (4, 'vanelle@mail.fr'  , 'Woumfo', 'Vanelle'  , '$2y$10$kRm7Chz1EhT48vLnF/kcT..aIHJ4vjOLKj/Rn5C/id/i7EmA6XjbW', 1),
       (5, 'tinhinane@mail.fr', 'Kadri' , 'Tinhinane', '$2y$10$Ax93Z4yWKOXAbSfQqTUw.etJSguKwuV3QTRrTOKvo8W2ogp3ZOl0W', 1),
       (6, 'toto@mail.fr'     , 'Toto'  , 'Toto'     , '$2y$10$LyaORAIphjJsoi49rrQp0unzX1M8PVRnJAzD2uXVjiJFELSB6kIua', 0),
       (7, 'tata@mail.fr'     , 'Tata'  , 'Tata'     , '$2y$10$QrJ6jtxipnyC8YpngGTE7Oj2dBKC7LzFbt6403Y4kQjlITUBl5yHW', 0);



INSERT INTO `categories` (`idCat`, `nom`)
VALUES (1, 'jeux-vidéos'),
       (2, 'livre'),
       (3, 'musique'),
       (4, 'peluche'),
       (5, 'fourniture de bureau'),
       (6, 'high-tech');

INSERT INTO `articles` (`idArticle`, `nom`, `idCat`, `prix`, `qte`, `img`)
VALUES (1, 'Counter-Strike: Global Offensive', 1, 12.50, 15, 'csgo.png'),
       (2, 'Portal', 1, 8.19, 20, 'portal.png'),
       (3, 'Portal 2', 1, 8.19, 15, 'portal2.png'),
       (4, 'Monster Hunter: World', 1, 29.99, 15, 'mhw.jpg'),
       (5, 'Final Fantasy VII Remake', 1, 69.99, 5, 'ff7r.jpg'),
       (6, 'Sea of Thieves', 1, 39.99, 25, 'sot.png'),
       (7, 'Journey', 1, 12.49, 20, 'journey.png'),
       (8, 'Biped', 1, 12.49, 18, 'biped.png'),
       (9, 'Tu vois j rap encore - Kery James', 3, 11.99, 20, 'tu_vois_jrap_encore_kery_james.png'),
       (10, 'La bonne école - Demi Portion', 3, 9.99, 25, 'la_bonne_ecole_demi_portion.jpg'),
       (11, 'L\'école du micro d\'argent - IAM', 3, 7.00, 5, 'IAM_ecole_du_micro_dargent.png'),
       (12, 'Yasuke - IAM', 3, 13.99, 10, 'yasuke_IAM.png'),
       (13, 'Art de rue - Fonky Family', 3, 10.00, 15, 'FFArtDeRue.jpg'),
       (14, '92.2012 - Kery James', 3, 15.00, 0, '92_2012_kery_james.jpg'),
       (15, 'Psykokwak collector Edition (80cm)', 4, 300.00, 1, 'psykokwak_peluche.png'),
       (16, 'Kiddo - Jessie Reyez', 3, 10.00, 16, 'kiddo_Jessie_Reyez.jpg'),
       (17, 'XP-PEN', 6, 349.00, 15, 'xp_pen.jpg'),
       (18, 'Je ne suis pas un serial killer - Dan Wells', 2, 15.00, 3, 'je_ne_suis_pas_un_serial_killer_dan_wells.jpg'),
       (19, 'Stylo Kiloutou', 5, 1.99, 10, NULL),
       (20, 'AirPods Pro', 6, 200.00, 4, 'airpods_pro.png'),
       (21, 'Tipp-Ex Rapid', 5, 1.49, 10, 'tipp-ex_rapid.png'),
       (22, 'Le compte de Monte Cristo', 2, 5.50, 5, 'le_comte_de_monte-christo_dumas.jpg'),
       (23, 'Le Robert', 2, 20.50, 5, 'le_robert.png'),
       (24, 'Eyeshield 21 - Tome 5', 2, 7.80, 5, 'Eyeshield_21_tome_5.jpg'),
       (25, 'Batman: La cour des hiboux', 2, 17.99, 5, 'batman_la_cour_des_hiboux.png'),
       (26, 'Anpanman (20cm)', 4, 16.89, 2, 'anpanman_peluche.png'),
       (27, 'Slime DQ', 4, 15.00, 1, 'dragon-quest-blue-slime-peluche.jpg'),
       (28, 'Skin - Flume (vinyle edition)', 3, 20.00, 10, 'skin_flume.jpg'),
       (29, 'Underclass Hero - Sum 41', 3, 10.00, 3, 'underclass_hero_sum_41.jpg'),
       (30, 'Oxygen Not Included', 1, 22.90, 3, 'oxygen_not_included.png'),
       (31, 'Goat Simulator - gold edition', 1, 8.19, 1, 'goat_simulator.png'),
       (32, 'The War: EXO', 3, 20, 10, 'the_war_exo.png');


INSERT INTO `commandes` (`idCommande`, `idUser`, `dateCmd`)
VALUES (1, 6, '2020-06-14 12:00:00'),
       (2, 6, '2020-05-15 18:45:56');

INSERT INTO `commandes_articles` (`idCommande`, `idArticle`, `qteCmd`)
VALUES (1, 11, 1),
       (1, 12, 1),
       (2, 21, 2),
       (2, 19, 5);


INSERT INTO `tri_par_defaut` (`colonne`, `croissant`) VALUES ('nom', 1);

INSERT INTO `paniers` (`idUser`, `idArticle`, `qte`)
VALUES (6, 1, 3),
       (6, 2, 2),
       (6, 3, 4),
       (6, 4, 3);
       